<?php

$panda=3;
$panda++;
$panda+=1;
var_dump($panda);
echo "<br/>";
--$panda;//cambia el valor antes de la linea
var_dump($panda);
$panda--;//cambia el valor despues de la linea
echo "<br/>";



//INTERPOLACION
echo "INTERPOLACION<br/>";
$valor='pandas';
$primero='Nos gustan los $valor';
$segundo= "Nos gustan los {$valor}";
var_dump($primero);
echo "<br/>";
var_dump($segundo);
echo "<br/>";
echo "<br/>";
echo "<br/>";



//CONCATENAR
echo "CONCATENAR<br/>";
$first = 'Los pandas son';
$second = "geniales";
$third = 2123;
$four = 1.23;
var_dump($first.$second.$third.$four);
echo "<br/>";
echo "<br/>";
echo "<br/>";
//MATRICES



echo "MATRICES<br/>";
$pandas=["Luis","Pedro","Thor"];
//matriz asociativa 
$numeros=[
	"uno" => 1,
	"dos" => 2,
	"tres" => 3,
	"cuatro" => 4,
	"cinco" =>5,
	"seis" =>6
];
echo $numeros["tres"];
echo "<br/>";
//Multidimencional
$numerosA =[
	"primos" => [2,3,5,7,11],
	"fibonacci" => [1,1,2,3,5],
	"Triangular" => [1,3,6,10,15]
];
//obtener el tercer valor de primos
$primos = $numerosA["primos"];
echo $primos[2];
echo "<br/>";
echo $numerosA["primos"][2];
echo "<br/>";
echo "<br/>";
echo "<br/>";


//CONVERSION DE TIPOS
echo "CONVERSION DE TIPOS<br/>";
$pandaA = "3";
$pandaA = (int)$pandaA;//convierte en entero
var_dump($pandaA);
echo "<br/>";
$pandaB = "Thor";
$pandaB = (array)$pandaB;//convierte en matriz
var_dump($pandaB);
echo "<br/>";
echo "<br/>";
echo "<br/>";





//BIFURCACIONES
echo "BIFURCACIONES<br/>";
//if
echo "if...<br/>";
$comp=0;
if($comp>0){
	echo "{$comp} es mayor que 0<br/>";
}else if($comp==0){
	echo "{$comp} es igual que 0<br/>";
}else{
	echo "{$comp} no es mayor que 0<br/>";
}
echo "<br/>";
echo "switch con cadena...<br/>";
$pandaC="Morocotudo";
switch ($pandaC) {
	case 'Algo':
		echo "el pandaC es igual a Algo";
		break;

	case 'Topo':
		echo "el pandaC es igual a Topo";
		break;
	
	default:
		echo "el pandaC es igual a {$pandaC}";
		break;
}
echo "<br/>";
echo "switch con enteros...<br/>";
$pandaD=123;
switch ($pandaD) {
	case 4:
		echo "el pandaD es igual a 4";
		break;

	case 5:
		echo "el pandaD es igual a 5";
		break;
	
	default:
		echo "el pandaD es igual a {$pandaD}";
		break;
}
echo "<br/>";
echo "<br/>";
echo "<br/>";




//BUCLES
echo "BUCLES<br/>";
echo "while...<br/>";
$pandaE = 0;
while ( $pandaE<= 10) {
	echo "El pandaE tiene el valor de : {$pandaE}<br/>";
	$pandaE++;
}
echo "<br/>";
echo"do while....<br/>";
echo "<br/>";
do{
	echo "El pandaE tiene el valor de : {$pandaE}<br/>";
	$pandaE++;

}while ( $pandaE<= 20) ;
	//echo "El pandaE tiene el valor de : {$pandaE}<br/>";
	//$pandaE++;
echo "<br/>";
echo "for...<br/>";
echo "<br/>";
for ($i=0; $i <$pandaE ; $i++) { 
	echo "El Indice i vale : {$i}<br/>";
}
echo "<br/>";
echo "foreach...<br/>";
echo "<br/>";
foreach ($pandas as $key) {
	echo "Hola {$key} !<br/>";
}
echo "<br/>";
echo "<br/>";
foreach ($numeros as $llave => $valor) {
	echo "eres el numero {$llave} ? ... si soy el numero {$valor}<br/>";
	//break; //corta la iteracion en el primer numero
}
echo "<br/>";
echo "<br/>";
foreach ($numeros as $llave => $valor) {
	if ($llave == "tres") {
		echo "no imprire el valor de tres<br/>";
		continue;
	}
	echo "eres el numero {$llave} ? ... si soy el numero {$valor}<br/>";
	//break; //corta la iteracion en el primer numero
}
echo "<br/>";
echo "<br/>";



?>